/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package messif.utility;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.algorithms.Algorithm;
import messif.algorithms.AlgorithmMethodException;
import messif.algorithms.impl.SequentialScan;
import messif.buckets.CapacityFullException;
import messif.objects.AbstractObject;
import messif.objects.LocalAbstractObject;
import messif.objects.util.RankedAbstractObject;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.operations.AnswerType;
import messif.operations.data.BulkInsertOperation;
import messif.operations.query.GetAllObjectsQueryOperation;
import messif.operations.query.KNNQueryOperation;

/**
 * Text-based protocol for answering queries on a sequantial scan algorithm.
 *
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 */
public class SimpleTCPTextAlgorithm {

    private Algorithm alg;
    private Map<String,Short> locToIdx;
    private ServerSocketChannel serverChnl;
    private AtomicBoolean canRun = new AtomicBoolean(true);
    
    private LocalAbstractObject.TextStreamFactory<? extends LocalAbstractObject> objFactory;

    private void loop(int defKnns) throws ClosedChannelException, IOException {
        Selector sel = Selector.open();
        serverChnl.register(sel, SelectionKey.OP_ACCEPT);
        String dataToSend = null;
        Request opReq = null;

        while (canRun.get()) {
            sel.select();
            Set<SelectionKey> selectedKeys = sel.selectedKeys();
            for (Iterator<SelectionKey> i = selectedKeys.iterator(); i.hasNext();) {
                SelectionKey k = i.next();

                // New connection
                if ((k.readyOps() & SelectionKey.OP_ACCEPT) == SelectionKey.OP_ACCEPT) {
                    Logger.getLogger(SimpleTCPTextAlgorithm.class.getName()).info("new connection");
                    i.remove();

                    //
                    // Retrieve the SocketChannel for the new connection, make it non-blocking, and register
                    // it with the same selector so now we can handle incoming data events on the same event
                    // loop
                    //
                    ServerSocketChannel ssc = (ServerSocketChannel) k.channel();
                    SocketChannel sc = ssc.accept();
                    sc.configureBlocking(false);
                    sc.register(sel, SelectionKey.OP_READ | SelectionKey.OP_WRITE);

                } else if ((k.readyOps() & SelectionKey.OP_READ) == SelectionKey.OP_READ) {
                    // New data available, read it
                    i.remove();

                    SocketChannel sc = (SocketChannel) k.channel();
                    ByteBuffer buffer = ByteBuffer.allocate(128 * 1024);
                    int bytesRead = sc.read(buffer);

                    if (bytesRead == -1) {
                        // TCP connection was closed
                        Logger.getLogger(SimpleTCPTextAlgorithm.class.getName()).info("TCP connection closed");
                        // Unregister the channel, by canceling the key. If we don't do this, data availability events for zero-length data will keep popping up.
                        k.cancel();

                    } else {
                        // Read data
                        int reqIdx = isRequestComplete(buffer);
                        if (reqIdx != -1) {
                            ByteBuffer nb = buffer.duplicate();
                            nb.flip();
                            nb.limit(reqIdx);
                            Request r = parseRequest(nb);
                            Logger.getLogger(SimpleTCPTextAlgorithm.class.getName()).info("received: " + r);
                            if (r != null) {
                                switch (r.type) {
                                    case QUIT:
                                        canRun.set(false);
                                        break;
                                    case SKIP:
                                        break;
                                    case SIZE:
                                        try {
                                            dataToSend = Integer.toString(alg.getObjectCount());
                                        } catch (AlgorithmMethodException ex) {
                                            dataToSend = "0";
                                        }
                                        break;
                                    case KNN:
                                        reqIdx = skipNewLineChars(reqIdx, buffer);
                                        nb = buffer.duplicate();
                                        nb.position(reqIdx);
                                        if (nb.remaining() == 0) {    // Wait for more data
                                            opReq = r;
                                            break;
                                        }
                                        LocalAbstractObject q;
                                        String res = null;
                                        for (int atmpt = 0; atmpt < 3; atmpt++) {
                                            q = parseObject(nb);
                                            if (q != null)
                                                res = processKnn(q, (r.knn == -1) ? defKnns : r.knn);
                                            if (res != null) {
                                                dataToSend = res;
                                                break;
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                        // continue reading data
                    }

                } else if ((k.readyOps() & SelectionKey.OP_WRITE) == SelectionKey.OP_WRITE) {
                    // Sth to send...
                    i.remove();

                    if (dataToSend != null) {
                        dataToSend += "\n";

                        SocketChannel sc = (SocketChannel) k.channel();
                        final ByteBuffer buf = ByteBuffer.wrap(dataToSend.getBytes());
                        while (buf.hasRemaining())
                            sc.write(buf);
                        dataToSend = null;
//                        ByteArrayOutputStream out = new ByteArrayOutputStream();
//                        object.write(out);
//                        final ByteBuffer buf = ByteBuffer.wrap(out.toByteArray());
//                        while (buf.hasRemaining())
//                            sc.write(buf);
                    } else {
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException ex) {
                            canRun.set(false);
                        }
                    }
                }
            }
        }
    }

    private String processKnn(LocalAbstractObject q, int k) {
        KNNQueryOperation op = new KNNQueryOperation(q, k);
        try {
            alg.executeOperation(op);
            
            StringBuilder sb = new StringBuilder();
            boolean first = true;
            for (Iterator<RankedAbstractObject> it = op.getAnswer(); it.hasNext(); ) {
                if (!first)
                    sb.append(',');
                RankedAbstractObject ro = it.next();
                short idx = locToIdx.getOrDefault(ro.getObject().getLocatorURI(), (short)-1);
                sb.append(idx).append(':').append(ro.getDistance());
                first = false;
            }
            sb.append('\n');
            return sb.toString();
        } catch (AlgorithmMethodException | NoSuchMethodException ex) {
            Logger.getLogger(SimpleTCPTextAlgorithm.class.getName()).log(Level.SEVERE, "Cannot execute kNN search!", ex);
        }
        return null;
    }
    
    private LocalAbstractObject parseObject(ByteBuffer buf) {
        String data = new String(buf.array(), buf.position(), buf.remaining());
        try {
            return objFactory.create(data);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(SimpleTCPTextAlgorithm.class.getName()).log(Level.SEVERE, "Cannot read a query object!", ex);
            return null;
        }
    }

    private int isRequestComplete(ByteBuffer buf) {
        final ByteBuffer resp = buf.duplicate();
        resp.flip();
        byte[] arr = resp.array();
        for (int i = 0; i < resp.limit(); i++) {
            if (arr[i] == 13 || arr[i] == 10) // cr OR lf
                return i;
        }
        return -1;
    }

    private int skipNewLineChars(int reqIdx, ByteBuffer buf) {
        final ByteBuffer resp = buf.duplicate();
        resp.flip();
        byte[] arr = resp.array();
        for (; reqIdx < resp.limit(); reqIdx++) {
            if (arr[reqIdx] != 13 && arr[reqIdx] != 10) // cr OR lf
                break;
        }
        return reqIdx;
    }

    private Request parseRequest(ByteBuffer buf) {
        String resp = new String(buf.array(), buf.position(), buf.remaining());
        
        if (resp.isEmpty())
            return new Request(Request.Type.SKIP, -1);
        
        if (resp.startsWith("size")) {
            return new Request(Request.Type.SIZE, -1);
        }
        if (resp.startsWith("knn")) {
            String[] knn = resp.split(":");
            return new Request(Request.Type.KNN, Integer.parseInt(knn[1]));
        }
        if (resp.startsWith("exit") || resp.startsWith("quit"))
            return new Request(Request.Type.QUIT, -1);

        return new Request(Request.Type.SKIP, -1);
    }

    private int run(String[] args) {
        try {
            String data = args[0];
            Class objCls = Class.forName(args[1]);
            int port = Integer.parseInt(args[2]);
            int defKnns = Integer.parseInt(args[3]);
            

            alg = new SequentialScan();
            alg.executeOperation(new BulkInsertOperation(new StreamGenericAbstractObjectIterator<>(objCls, data)));
            GetAllObjectsQueryOperation opAllObjs = new GetAllObjectsQueryOperation(AnswerType.ORIGINAL_OBJECTS);
            alg.executeOperation(opAllObjs);
            locToIdx = new HashMap<>();
            short idx = 0;
            for (Iterator<AbstractObject> it = opAllObjs.iterator(); it.hasNext(); idx++) {
                AbstractObject o = it.next();
                locToIdx.put(o.getLocatorURI(), idx);
            }
           
            objFactory = new LocalAbstractObject.TextStreamFactory<LocalAbstractObject>(objCls);

            serverChnl = ServerSocketChannel.open();
            serverChnl.configureBlocking(false);
            InetSocketAddress address = new InetSocketAddress(port);
            ServerSocket ss = serverChnl.socket();
            ss.bind(address);

            loop(defKnns);

        } catch (CapacityFullException | InstantiationException | AlgorithmMethodException | NoSuchMethodException | IllegalArgumentException | IOException | ClassNotFoundException ex) {
            Logger.getLogger(SimpleTCPTextAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
            return 2;
        }
        return 0;
    }
    
    public static void main(String[] args) {
        if (args.length != 4) {
            System.out.println("Usage: SimpleTCPTextAlgorithm <data_file> <object_class> <port> <knns>");
            System.out.println("  Parameters: <data_file> -- path to messif-serialized data file with objects.");
            System.out.println("              <object_class> -- class of objects to load from the passed data file.");
            System.out.println("              <port> -- port number to listen on.");
            System.out.println("              <knns> -- defualt number of neighbors to return if the query requestes -1.");
            return;
        }

        SimpleTCPTextAlgorithm srv = new SimpleTCPTextAlgorithm();
        srv.run(args);
    }

    private static class Request {
        public enum Type { QUIT, SKIP, SIZE, KNN };
        public Type type;
        public int knn;

        public Request(Type type, int knn) {
            this.type = type;
            this.knn = knn;
        }

        @Override
        public String toString() {
            return "Request{" + "type=" + type + ", knn=" + knn + '}';
        }
    }
}
