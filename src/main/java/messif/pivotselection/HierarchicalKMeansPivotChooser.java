/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messif.pivotselection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.util.AbstractObjectList;

/**
 * Hierarchical k-means/medoids pivot chooser. You have to pass the numbers of pivots per level in the constructor and the flag whether
 * the pivots are selected (false) or computed (true) as centers.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class HierarchicalKMeansPivotChooser extends AbstractPivotChooser {

    /** Pivots of K-means per level. It is also used to calculate the number of levels. 
     * The parameter 'pivot count' to {@link #selectPivot(int)} is ignored. */
    private int[] pivotsPerLevel;
    
    /** K-means strategy of selecting (actually computing) new centroids */
    private boolean useKmeansForCenters;
    
    /**
     * Creates a new instance of HierarchicalKMeansPivotChooser with the passed number of levels and the number of centers per level.
     */
    public HierarchicalKMeansPivotChooser(String pivotsPerLevel, boolean useKmeansForCenters) {
        this.pivotsPerLevel = parseToArray(pivotsPerLevel);
        this.useKmeansForCenters = useKmeansForCenters;
    }
    
    private int[] parseToArray(String s) {
        try {
            String[] parts = s.split(",\\s*");
            int[] res = new int[parts.length];
            for (int i = 0; i < parts.length; i++) {
                res[i] = Integer.parseInt(parts[i]);
            }
            return res;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Incorrect pivot counts per level. Comma-separated list expected! Passed '" + s + "'.");
        }
    }

    private int getLevelCount() {
        return pivotsPerLevel.length;
        //return (int)Math.ceil(Math.log10(totalPivotCount) / Math.log10(pivotsPerLevel));
    }
    
    @Override
    protected void selectPivot(int count, AbstractObjectIterator<? extends LocalAbstractObject> sampleSetIterator) {
        
        // Store all passed objects temporarily
        AbstractObjectList<LocalAbstractObject> objectList = new AbstractObjectList<LocalAbstractObject>(sampleSetIterator);
        
        List<LocalAbstractObject> allPivots = new ArrayList<LocalAbstractObject>();
        int levels = getLevelCount();
        
        // For each level and a partition there, run k-means chooser
        List<ChooserDataPair> subChoosers = optimizeFirstLevel(objectList, 5, allPivots); // first (root) chooser (optimize it from several attempts)
        for (int lvl = 1; lvl < levels; lvl++) {
            int iterationNo = 0;
            List<ChooserDataPair> nextChoosers = new ArrayList<>(subChoosers.size() * pivotsPerLevel[lvl]);
            for (ChooserDataPair pair : subChoosers) {
                System.err.println("### Level " + (lvl + 1) + ", partition No. " + (iterationNo + 1) + ", pivots to choose " + (pivotsPerLevel[lvl]));
                pair.chooser.selectPivot(pivotsPerLevel[lvl], pair.objects.iterator());
                // Get all partitions for the next levels
                for (Iterator<AbstractObjectList<LocalAbstractObject>> iterParts = pair.chooser.iteratorPartitions(); iterParts.hasNext(); ) {
                    nextChoosers.add(new ChooserDataPair(new KMeansPivotChooser(useKmeansForCenters), iterParts.next()));
                }
                // Add pivots to the result
                for (Iterator<LocalAbstractObject> iter = pair.chooser.iterator(); iter.hasNext(); ) {
                    allPivots.add(iter.next());
                }
                ++iterationNo;
            }
            subChoosers = nextChoosers; // Swap structures for the next iteration
        }
        
        // Copy to results
        preselectedPivots.addAll(allPivots);
    }
    
    /** Test several k-means (by the parameter) and return the best one */
    private List<ChooserDataPair> optimizeFirstLevel(AbstractObjectList<LocalAbstractObject> objectList, int steps, List<LocalAbstractObject> allPivots) {
        KMeansPivotChooser best = null;
        float bestVar = Float.MAX_VALUE;
        for (int stepNo = 0; stepNo < steps; stepNo++) {
            System.err.println("### Level 1, partition No. 1, step No. " + (stepNo + 1) + ", pivots to choose " + (pivotsPerLevel[0]));
            KMeansPivotChooser chooser = new KMeansPivotChooser(useKmeansForCenters);
            chooser.selectPivot(pivotsPerLevel[0], objectList.iterator());
            // Get variance over all partitions for the next levels
            long sum = 0;
            long squares = 0;
            long cnt = 0;
            for (Iterator<AbstractObjectList<LocalAbstractObject>> iterParts = chooser.iteratorPartitions(); iterParts.hasNext(); ) {
                long s = (long)iterParts.next().size();
                sum += s;
                squares += s*s;
                cnt++;
            }
            float variance = ((float)squares - (float)(sum*sum)/(float)cnt) / (float)(cnt - 1);
            if (variance < bestVar) {
                best = chooser;
                bestVar = variance;
                System.err.println("###                      this was better");
            }
        }
        List<ChooserDataPair> subChoosers = new ArrayList<>();
        // Get all partitions for the next levels
        for (Iterator<AbstractObjectList<LocalAbstractObject>> iterParts = best.iteratorPartitions(); iterParts.hasNext(); ) {
            subChoosers.add(new ChooserDataPair(new KMeansPivotChooser(useKmeansForCenters), iterParts.next()));
        }
        // Add selected pivots to the result
        for (Iterator<LocalAbstractObject> iter = best.iterator(); iter.hasNext(); ) {
            allPivots.add(iter.next());
        }
        return subChoosers;
    }
    
    private class ChooserDataPair {
        KMeansPivotChooser chooser;
        AbstractObjectList<LocalAbstractObject> objects;

        public ChooserDataPair(KMeansPivotChooser chooser, AbstractObjectList<LocalAbstractObject> objects) {
            this.chooser = chooser;
            this.objects = objects;
        }
    }
}
