/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.pivotselection;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import messif.objects.LocalAbstractObject;
import messif.objects.PrecompDistPerforatedArrayFilter;
import messif.objects.impl.ObjectFloatVectorNeuralNetworkL2;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.util.AbstractObjectList;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.objects.ObjectVector;
import messif.objects.keys.AbstractObjectKey;


/**
 * This class uses the k-means algorithm adapted for metric spaces to cluster the objects, 
 * so it is <strong>k-medoids</strong> algorithm in fact. The difference of this k-medoids to Partitioning Around Medoids (PAM) is
 * in selecting new cluster medoids, which we do not do exhaustivelly as done in PAM -- PAM uses also candidates out of the cluster itself, 
 * whereas we take just cluster objects, so we may end up in a local optimum more likely than PAM.
 * 
 * New cluster's center is selected as an object that minimizes the sum of squared distances to other objects in the cluster.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class KMeansPivotChooser extends AbstractPivotChooser {
    
    /** Size of the sample set to select a pivot from in each iteration of the k-means */
    public static int PIVOTS_SAMPLE_SIZE = 1000;
    
    /** Threshold to consider 2 pivots the same */
    public static float PIVOTS_DISTINCTION_THRESHOLD = 0.1f;
    
    /** Maximal number of iterations to let run */
    public static int MAX_ITERATIONS = 100;
    /** Default number of threads to use in voronoi partitioning */
    public static int MAX_THREADS = 10;
    
    /** List of initial pivots */
    protected AbstractObjectList<LocalAbstractObject> initialPivots;
    
    /** K-means strategy of selecting (actually computing) new centroids */
    private boolean useKmeansForCenters;
    
    /** Partitioning (clustering) done by this pivot chooser */
    private List<AbstractObjectList<LocalAbstractObject>> resultingPartitioning;
    
    /**
     * Creates a new instance of KMeansPivotChooser with empty initial list of pivots.
     * Cluster centers are selected as a medoid (so it is not computed!)
     */
    public KMeansPivotChooser() {
        this(null);
    }

    public KMeansPivotChooser(boolean useKmeansForCenters) {
        this(null);
        this.useKmeansForCenters = useKmeansForCenters;
    }
    
    /**
     * Creates a new instance of KMeansPivotChooser.
     * @param initialPivots the list of initial pivots
     */
    public KMeansPivotChooser(AbstractObjectList<LocalAbstractObject> initialPivots) {
        this.initialPivots = initialPivots;
        this.useKmeansForCenters = false;
    }

    public List<AbstractObjectList<LocalAbstractObject>> getClusters() {
        return resultingPartitioning;
    }
    
    /**
     *  This method only uses the preselected pivots as initial pivots for k-means and rewrites the pivots completely
     */
    @Override
    protected void selectPivot(int count, AbstractObjectIterator<? extends LocalAbstractObject> sampleSetIterator) {
        long timeStart = 0;
        
        // Thread pool for possible parallelism in partitioning objects to cell centers
        ExecutorService pool = Executors.newFixedThreadPool(MAX_THREADS);

        System.err.println("Selecting pivots using " + ((useKmeansForCenters) ? "k-means" : "k-medoids"));
        System.err.println("Each iteration will use " + MAX_THREADS + " threads to assignment of objects to partitions.");
        
        // Store all passed objects temporarily
        AbstractObjectList<LocalAbstractObject> objectList = new AbstractObjectList<LocalAbstractObject>(sampleSetIterator);
        
        boolean anyEmpty = true;
        int emptyMaxTries = 5;
        List<LocalAbstractObject> pivots = null;
        List<AbstractObjectList<LocalAbstractObject>> actualClusters = null; 
        while (anyEmpty && --emptyMaxTries > 0) {
            timeStart = System.currentTimeMillis();     // Do not count any "reselection" of pivots if it lead to any empty cluster.
            pivots = initPivots(count, objectList);
            actualClusters = getPartitioning(objectList, pivots, "initial");
            anyEmpty = isClusterEmpty(actualClusters);
            if (anyEmpty)
                System.err.println("WARNING: There is at least one empty cluster after the random selection.");
        }
        
        boolean continueKMeans = true;
        
        // one step of the k-means algorithm
        int nIterations = 0;
        List<AbstractObjectList<LocalAbstractObject>> prevClusters = null;
        while (continueKMeans && (nIterations++ < MAX_ITERATIONS)) {
            System.err.println("Running "+nIterations+"th iteration"); 
            
            // Compute data partitioning and report cluster sizes
            if (actualClusters == null)
                actualClusters = getPartitioning(objectList, pivots, "intermediate");
            
            System.err.println("    Selecting clusteroids...");
            // now calculate the new pivots for the new clusters
            int i = 0;
            List<Future<CenterTaskResult>> selectingThreads = new ArrayList<>();
//            List<CenterThread> selectingThreads = new ArrayList<CenterThread>();
            for (AbstractObjectList<LocalAbstractObject> cluster : actualClusters) {
//                CenterTask thread = (useKmeansForCenters) ? new ComputeCentroidThread(cluster, pivots.get(i++))
//                                                            : new SelectClusteroidThread(cluster, pivots.get(i++));
//                thread.start();
//                selectingThreads.add(thread);
                CenterTask task = (useKmeansForCenters) ? new ComputeCentroidThread(cluster, i, pivots.get(i++))
                                                            : new SelectClusteroidThread(cluster, i, pivots.get(i++));
                selectingThreads.add(pool.submit(task));
            }
            
            continueKMeans = false;
            boolean allDone = false;
            while (!allDone) {
                allDone = true;
                for (Future<CenterTaskResult> f : selectingThreads) {
                    if (!f.isDone()) {
                        allDone = false;
                        break;
                    }
                }
                if (!allDone) {
                    try { 
                        Thread.sleep(10);
                    } catch (InterruptedException ex) {
                        break;
                    }
                    continue;
                }
                
                // Process the result of tasks.
                i = 0;
                for (Future<CenterTaskResult> f : selectingThreads) {
                    try {
                        CenterTaskResult result = f.get();
                        if (result == null) {
                            System.err.println("        WARNING: no clusteroid selected - empty cluster?: "+ actualClusters.get(i).size());
                            //System.out.println("          selecting the pivot at random");
                            continueKMeans = true;
                        } else {
                            float pivotShiftDist = result.originalPivot.getDistance(result.newPivot);
                            if (pivotShiftDist > PIVOTS_DISTINCTION_THRESHOLD) {
                                System.err.println("        pivot "+ result.clusterIdx +" shifted by "+pivotShiftDist);
                                pivots.set(result.clusterIdx, result.newPivot);
                                continueKMeans = true;
                            }
                        }
                    } catch (InterruptedException | ExecutionException ex) {
                        ex.printStackTrace();
                    }
                }
                
//                for (CenterTask thread : selectingThreads) {
//                    try {
//                        thread.join();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    if (thread.clusteroid == null) {
//                        System.err.println("        WARNING: no clusteroid selected - empty cluster?: "+ actualClusters.get(i).size());
//                        //System.out.println("          selecting the pivot at random");
//                        continueKMeans = true;
//                    } else {
//                        float pivotShiftDist = pivots.get(i).getDistance(thread.clusteroid);
//                        if (pivotShiftDist > PIVOTS_DISTINCTION_THRESHOLD) {
//                            System.err.println("        pivot "+ i +" shifted by "+pivotShiftDist);
//                            pivots.set(i, thread.clusteroid);
//                            continueKMeans = true;
//                        }
//                    }
//                    i++;
//                }
            }
            // Test cluster size -- if all the same, stop.
            if (continueKMeans && prevClusters != null) {
                boolean allEqualSize = true;
                for (int j = 0; allEqualSize && (j < actualClusters.size()); j++) {
                    allEqualSize = allEqualSize && (prevClusters.get(j).size() == actualClusters.get(j).size());
                }
                continueKMeans = !allEqualSize;
            }
            
            //printPivots("Current pivots:", pivots);
            prevClusters = actualClusters;
            actualClusters = null;
        }
        
        long timeStop = System.currentTimeMillis();
        
        // Compute data partitioning and report final cluster sizes
        resultingPartitioning = getPartitioning(objectList, pivots, "final");
        
        //this.preselectedPivots.clear();
        for (LocalAbstractObject pivot : pivots)
            preselectedPivots.add(pivot);
        
        System.err.println("KMeans took " + (timeStop - timeStart) + " ms");
        List<Runnable> poolRemaining = pool.shutdownNow();
        if (poolRemaining != null && !poolRemaining.isEmpty()) {
            System.err.println("Warning: Terminating thread pool with " + poolRemaining.size() + " jobs active/queued!");
        }
    }

    private List<LocalAbstractObject> initPivots(int count, AbstractObjectList<LocalAbstractObject> objectList) {
        List<LocalAbstractObject> pivots = new ArrayList<LocalAbstractObject>(count);
        // initially select "count" pivots at random - or use (partly) preselected pivots
        if (initialPivots != null) {
            for (LocalAbstractObject preselPivot : initialPivots) {
                if (count > pivots.size()) {
                    pivots.add(preselPivot);
                    System.err.println("Adding preselected pivot: "+preselPivot.getLocatorURI());
                }
            }
        }
        if (count > pivots.size()) {
            System.err.println("Selecting: "+(count - pivots.size()) +" pivots at random");
            pivots.addAll(objectList.randomList(count - pivots.size(), true, new AbstractObjectList<LocalAbstractObject>()));
        }
        //printPivots("Initial pivots:", pivots);
        return pivots;
    }
    
    public Iterator<AbstractObjectList<LocalAbstractObject>> iteratorPartitions() {
        return resultingPartitioning.iterator();
    }

    private List<AbstractObjectList<LocalAbstractObject>> getPartitioning(AbstractObjectList<LocalAbstractObject> objectList, 
                                                                            List<LocalAbstractObject> pivots, String msg) {
        List<AbstractObjectList<LocalAbstractObject>> actualClusters;
        System.err.print("    Voronoi partitioning... ");
        actualClusters = voronoiLikePartitioning(objectList, pivots);
        System.err.println("done");
        StringBuilder buf = new StringBuilder("       " + msg + " cluster sizes:");
        for (AbstractObjectList<LocalAbstractObject> cluster : actualClusters) {
            buf.append(" ").append(cluster.size());
        }
        System.err.println(buf.toString());
        return actualClusters;
    }

    /**
     * Prints all pivots selected by this chooser. Pivots are printed to <code>System.err</code>.
     * @param msg optional message printed before the pivots
     */
    public void printPivots(String msg) {
        printPivots(msg, preselectedPivots);
    }
    
    private void printPivots(String msg, List<LocalAbstractObject> pivots) {
        if (msg != null)
            System.err.println(msg);
        int i = 0;
        for (LocalAbstractObject p : pivots) {
            System.err.println("Pivot " + (++i) + ": " + p);
        }
    }

    /** Given a set of objects and a set of pivots, cluster the dataset.
     * @return list of abstract object lists, where each abstract object list corresponds to one cluster.
     */
    private List<AbstractObjectList<LocalAbstractObject>> voronoiLikePartitioning(AbstractObjectList<LocalAbstractObject> objects, 
                                                                                    List<LocalAbstractObject> pivots) {
        List<AbstractObjectList<LocalAbstractObject>> clusters =  new ArrayList<AbstractObjectList<LocalAbstractObject>>(pivots.size());
        
        //  init the cluster for this pivot
        for (LocalAbstractObject pivot : pivots) {
            clusters.add(new AbstractObjectList<LocalAbstractObject>());

//            // precompute the mutual distances between the pivots
//            PrecompDistPerforatedArrayFilter filter = pivot.getDistanceFilter(PrecompDistPerforatedArrayFilter.class);
//            if (filter == null) {
//                filter = new PrecompDistPerforatedArrayFilter(pivots.size());
//                pivot.chainFilter(filter, true);
//            } else filter.resetAllPrecompDist();
//            
//            for (LocalAbstractObject pivot2 : pivots) {
//                filter.addPrecompDist(pivot.getDistance(pivot2));
//            }
        }
        
        // Assign objects to the cluster of the closest pivot
        for (LocalAbstractObject object : objects) {
//            PrecompDistPerforatedArrayFilter filter = object.getDistanceFilter(PrecompDistPerforatedArrayFilter.class);
//            if (filter == null) {
//                filter = new PrecompDistPerforatedArrayFilter(pivots.size());
//                object.chainFilter(filter, true);
//            } else filter.resetAllPrecompDist();
            
            float minDistance = Float.MAX_VALUE;
            int i = 0;
            int closestPivotIdx = -1;
            int closestClusterSize = Integer.MAX_VALUE;
            float objPivotDist;
            for (LocalAbstractObject pivot : pivots) {
//                if (object.excludeUsingPrecompDist(pivot, minDistance))
//                    filter.addPrecompDist(LocalAbstractObject.UNKNOWN_DISTANCE);
//                else {
                    objPivotDist = object.getDistance(pivot);
//                    filter.addPrecompDist(objPivotDist);
                    if (minDistance > objPivotDist) {
                        closestPivotIdx = i;
                        minDistance = objPivotDist;
                        closestClusterSize = clusters.get(closestPivotIdx).size() + 1;
                    } else if (minDistance == objPivotDist && closestPivotIdx != -1) {     // On ties, choose the smaller cluster.
                        if (closestClusterSize > clusters.get(closestPivotIdx).size()) {
                            closestPivotIdx = i;
                            minDistance = objPivotDist;
                            closestClusterSize = clusters.get(closestPivotIdx).size() + 1;
                        } else {
                            System.err.println("WARNING: There is another pivot at idx " + i + " at the distance " + minDistance
                                    + " same as the distance from the currently closest pivot at idx " + closestPivotIdx + ". Also smaller cluster size did not break the tie. Using the closest cluster at idx " + closestPivotIdx);
                        }
                    }
//                }
                i++;
            }
            clusters.get(closestPivotIdx).add(object);
        }
        
        return clusters;
    }

    private boolean isClusterEmpty(List<AbstractObjectList<LocalAbstractObject>> actualClusters) {
        for (AbstractObjectList<LocalAbstractObject> cluster : actualClusters) {
            if (cluster.isEmpty())
                return true;
        }
        return false;
    }
    
    private static class CenterTaskResult {
        int clusterIdx;
        LocalAbstractObject originalPivot;
        LocalAbstractObject newPivot;

        public CenterTaskResult(int clusterIdx, LocalAbstractObject originalPivot, LocalAbstractObject newPivot) {
            this.clusterIdx = clusterIdx;
            this.originalPivot = originalPivot;
            this.newPivot = newPivot;
        }
    }
    
    /** Internal abract thread for selecting new "center" of a cluster. */
    public static abstract class CenterTask implements Callable<CenterTaskResult> {
        // parameter
        AbstractObjectList<LocalAbstractObject> cluster;
        
        // always try the original pivot - put it to the sample pivots list
        LocalAbstractObject originalPivot;
        int clusterIdx;
        
        // result
        LocalAbstractObject clusteroid;
        
        /**
         * Creates a new thread for computing the "center" of a cluster.
         * @param cluster the list of objects that form a cluster
         * @param originalPivot the original pivot that is improved
         */
        protected CenterTask(AbstractObjectList<LocalAbstractObject> cluster, int clusterIdx, LocalAbstractObject originalPivot) {
            this.cluster = cluster;
            this.originalPivot = originalPivot;
            this.clusterIdx = clusterIdx;
        }

        public LocalAbstractObject getClustroid() {
            return clusteroid;
        }
        
        @Override
        public CenterTaskResult call() {
            AbstractObjectList<LocalAbstractObject> samplePivots;
                    
            if (cluster.size() <= PIVOTS_SAMPLE_SIZE || PIVOTS_SAMPLE_SIZE == -1) {    // Use all objects
                samplePivots = cluster;
            } else {
                samplePivots = cluster.randomList(PIVOTS_SAMPLE_SIZE, true, new AbstractObjectList<LocalAbstractObject>(PIVOTS_SAMPLE_SIZE));
                boolean found = false;
                for (LocalAbstractObject p : samplePivots) {
                    if (p.getLocatorURI().equals(this.originalPivot.getLocatorURI())) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    System.err.println("        Adding original pivot to sample objects.");
                    samplePivots.add(this.originalPivot);
                }
            }
            if (samplePivots == null || samplePivots.isEmpty())
                return null;
            
            obtainCenter(samplePivots);
            
            // Report the result
            return new CenterTaskResult(clusterIdx, originalPivot, clusteroid);
        }

        /** Compute/find new centroid/center and assign it to @{link #clusteroid}.
         * @param samplePivots list of objects to be used to get new center from
         */
        protected abstract void obtainCenter(AbstractObjectList<LocalAbstractObject> samplePivots);
    }
    
    private static final AtomicInteger computedObjId = new AtomicInteger();
    /** Thread for computing a new artifical cluster center as a mean vector of all passed vectors. */
    public static class ComputeCentroidThread extends CenterTask {

        public ComputeCentroidThread(AbstractObjectList<LocalAbstractObject> cluster, int clusterIdx, LocalAbstractObject originalPivot) {
            super(cluster, clusterIdx, originalPivot);
        }

        @Override
        protected void obtainCenter(AbstractObjectList<LocalAbstractObject> samplePivots) {
            if (!(originalPivot instanceof ObjectVector))
                throw new UnsupportedOperationException("Local abstract object's class must implement ObjectVector interface. Class " + ((originalPivot == null) ? "null" : originalPivot.getClass()) + " is not supported yet.");
            
            float[] newdata = null;
            for (LocalAbstractObject o : samplePivots) {
                final int dim = ((ObjectVector)o).getVectorDataDimension();
                if (newdata == null)
                    newdata = new float[dim];
                for (int i = 0; i < newdata.length; i++)
                    newdata[i] += ((ObjectVector)o).getVectorDataItem(i).floatValue();
            }
            for (int i = 0; i < newdata.length; i++)
                newdata[i] /= samplePivots.size();
            
            clusteroid = newInstance(originalPivot.getClass(), String.format("computed_%d", computedObjId.incrementAndGet()), newdata);
            //clustroid = new ObjectFloatVectorNeuralNetworkL2(String.format("computed_%d", computedObjId.incrementAndGet()), newdata);
        }

        private <T> LocalAbstractObject newInstance(Class<? extends LocalAbstractObject> cls, String locator, float[] data) {
            Class<?>[] types = new Class<?>[] { float.class, double.class, byte.class, short.class, int.class, long.class };
            Constructor<? extends LocalAbstractObject> constr;
            for (Class<?> type : types) {
                try {
                    constr = cls.getConstructor(type);
                    final LocalAbstractObject o = constr.newInstance(newInstanceConvertData(data, type));
                    o.setObjectKey(new AbstractObjectKey(locator));
                    return o;
                } catch (NoSuchMethodException ex) {
                    Logger.getLogger(KMeansPivotChooser.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SecurityException ex) {
                    Logger.getLogger(KMeansPivotChooser.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    Logger.getLogger(KMeansPivotChooser.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(KMeansPivotChooser.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(KMeansPivotChooser.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    Logger.getLogger(KMeansPivotChooser.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return null;
        }
    
        private <T> T[] newInstanceConvertData(float[] data, Class<?> clz) {
            T[] arr = (T[]) Array.newInstance(clz, data.length);
            for (int i = 0; i < data.length; i++)
                arr[i] = (T)(Number)data[i];
            return arr;
        }
    }
    
    /** Thread for selecting the "center" of a cluster. */
    public static class SelectClusteroidThread extends CenterTask {
        /**
         * Creates a new SelectClustroidThread for computing the "center" of a cluster.
         * @param cluster the list of objects that form a cluster
         * @param originalPivot the original pivot that is improved
         */
        public SelectClusteroidThread(AbstractObjectList<LocalAbstractObject> cluster, int clusterIdx, LocalAbstractObject originalPivot) {
            super(cluster, clusterIdx, originalPivot);
        }

        @Override
        protected void obtainCenter(AbstractObjectList<LocalAbstractObject> samplePivots) {
            double minRowSum = Double.MAX_VALUE;
            for (LocalAbstractObject pivot : samplePivots) {
                double rowSum = 0;
                for (LocalAbstractObject object : cluster) {
                    rowSum += Math.pow(pivot.getDistance(object), 2);
                }
                if (minRowSum > rowSum) {
                    clusteroid = pivot;
                    minRowSum = rowSum;
                }
            }
        }
    }
}
