/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package messif.operations.data;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import messif.buckets.BucketErrorCode;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectList;
import messif.objects.util.GenericMatchingObjectList;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.operations.AbstractOperation;
import static messif.operations.data.BulkInsertOperation.INSERTED_OBJECTS_PARAM;
import static messif.utility.DirectoryInputStream.searchFiles;

/**
 * Operation for inserting several lists of objects at once.
 * The operation keeps a map of partion ID and a list of {@link messif.objects.AbstractObject abstract objects}
 * that are going to be inserted into an index structure.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
@AbstractOperation.OperationName("Bulk insert of already partitioned data")
public class PartitionedBulkInsertOperation<E extends LocalAbstractObject> extends DataManipulationOperation {
    /** Class serial id for serialization */
    private static final long serialVersionUID = 1L;

    //****************** Attributes ******************//

    /** List of objects to insert split into individual partitions */
    private GenericMatchingObjectList<? extends LocalAbstractObject> insertedObjects;


    //****************** Constructors ******************//

    /**
     * Creates a new instance of BulkInsertOperation.
     * Empty collection is <em>not</em> permitted.
     *
     * @param objClass the class used to create the instances of objects in this stream
     * @param pathToDataDir a directory where all files are taken and read in as partitioned data to be inserted
     * @throws NoSuchElementException if the directory does not exist or no files were read from it
     */
    @AbstractOperation.OperationConstructor({"Class of objects to instantiate", "path to directory with files to read in as data to insert"})
    public PartitionedBulkInsertOperation(Class<? extends E> objClass, String pathToDataDir) throws NoSuchElementException, IOException {
        // Prepare iterator on file names
        List<File> files = new ArrayList<File>();
        searchFiles(files, new File(pathToDataDir), new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isFile();
            }
        }, false);
        Collections.sort(files);
        
        insertedObjects = new GenericMatchingObjectList<>();
        
        for (int partId = 0; partId < files.size(); partId++) {
            final StreamGenericAbstractObjectIterator<E> it = (StreamGenericAbstractObjectIterator<E>)new StreamGenericAbstractObjectIterator<>(objClass, files.get(partId).getPath());
            insertedObjects.put(partId, (AbstractObjectList)new AbstractObjectList<E>(it));
        }
    }
    
//    /**
//     * Creates a new instance of BulkInsertOperation.
//     * Empty collection is <em>not</em> permitted.
//     *
//     * @param insertedObjects a list of objects to be inserted by this operation
//     * @param permitEmpty flag whether the empty list of objects to insert is permitted
//     *          (<tt>true</tt>) or a {@link NoSuchElementException} is thrown (<tt>false</tt>)
//     * @throws NoSuchElementException if the inserted objects list is empty 
//     */
//    public PartitionedBulkInsertOperation(Collection<? extends LocalAbstractObject> insertedObjects) throws NoSuchElementException {
//        this(new ArrayList<LocalAbstractObject>(insertedObjects), permitEmpty);
//    }
//    
//    /**
//     * Creates a new instance of BulkInsertOperation from all objects provided by the iterator.
//     * Empty collection is <em>not</em> permitted.
//     * 
//     * @param insertedObjects a list of objects to be inserted by this operation
//     * @throws NoSuchElementException if the inserted objects list is empty 
//     */
//    @AbstractOperation.OperationConstructor({"Iterator of objects to insert"})
//    public PartitionedBulkInsertOperation(Iterator<? extends LocalAbstractObject> insertedObjects) throws NoSuchElementException {
//        this(new AbstractObjectList<LocalAbstractObject>(insertedObjects), false);
//    }

    //****************** Attribute access method ******************//

    /**
     * Returns the list of objects to insert from the passed partition ID.
     * @return the list of objects to insert from the passed partition ID
     */
    public List<? extends LocalAbstractObject> getInsertedObjects(int partId) {
        return Collections.unmodifiableList(insertedObjects.get(partId));
    }

    /**
     * Return the number of objects to be inserted from the passed partition ID by this bulk insert operation.
     * @return the number of objects to be inserted from the passed partition ID by this bulk insert operation
     */
    public int getNumberInsertedObjects(int partId) {
        return insertedObjects.getObjectCount(partId);
    }

    
    @Override
    public Object getArgument(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index >= getArgumentCount())
            throw new IndexOutOfBoundsException("PartitionedBulkInsertOperation has arguments equal to the number of partitions, i.e., " + getArgumentCount());
        return getInsertedObjects(index);
    }

    @Override
    public int getArgumentCount() {
        return insertedObjects.size();
    }
    
    //****************** Overrides ******************//

    @Override
    public boolean wasSuccessful() {
        return isErrorCode(BucketErrorCode.OBJECT_INSERTED, BucketErrorCode.SOFTCAPACITY_EXCEEDED);
    }

    @Override
    public void endOperation() {
        endOperation(BucketErrorCode.OBJECT_INSERTED);
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer().append("PartitionedBulkInsertOperation (").append(insertedObjects.size()).append(" objects) ");
        buffer.append(isFinished() ? "HAS" : "NOT").append(" finished: ");
        if (isFinished()) {
            buffer.append(getErrorCode().toString());
            if (getParameter(INSERTED_OBJECTS_PARAM) != null && (getParameter(INSERTED_OBJECTS_PARAM) instanceof Collection)) {
                buffer.append(" (inserted ").append(((Collection) getParameter(INSERTED_OBJECTS_PARAM)).size()).append(" objects)");
            }
        }
        return buffer.toString();
    }

    @Override
    public void clearSurplusData() {
        super.clearSurplusData();
        // TODO: solve this better
        insertedObjects.clear();
        //for (LocalAbstractObject object : insertedObjects)
        //    object.clearSurplusData();
    }

    //****************** Cloning ******************//

    @Override
    public PartitionedBulkInsertOperation clone() throws CloneNotSupportedException {
        PartitionedBulkInsertOperation operation = (PartitionedBulkInsertOperation)super.clone();
        if (operation.insertedObjects != null) {
            GenericMatchingObjectList<LocalAbstractObject> matchingList = new GenericMatchingObjectList<LocalAbstractObject>();
            for (int partId = 0; partId < insertedObjects.size(); partId++) {
                AbstractObjectList<E> lst = new AbstractObjectList<>();
                operation.insertedObjects.put(partId, lst);
                for (Iterator<E> it = (Iterator)insertedObjects.iterator(partId); it.hasNext();) {
                    lst.add((E)(it.next().clone()));
                }
            }
            operation.insertedObjects = matchingList;
        }
        return operation;
    }
}
