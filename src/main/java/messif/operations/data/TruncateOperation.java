/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messif.operations.data;

import messif.buckets.BucketErrorCode;
import messif.operations.AbstractOperation;

/**
 * Operation for clearing all object(s).
 * The operation keeps a list of {@link messif.objects.AbstractObject abstract object}.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
@AbstractOperation.OperationName("Truncate")
public class TruncateOperation extends DataManipulationOperation {
    
    /** Class serial id for Java serialization. */
    private static final long serialVersionUID = 1L;
    
    /**
     * Creates a new instance of TruncateOperation.
     */
    @AbstractOperation.OperationConstructor({})
    public TruncateOperation() {
    }

    @Override
    public boolean wasSuccessful() {
        return isErrorCode(BucketErrorCode.OBJECT_DELETED);
    }

    @Override
    public void endOperation() {
        endOperation(BucketErrorCode.OBJECT_DELETED);
    }

}
