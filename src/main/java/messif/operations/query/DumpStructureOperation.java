/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messif.operations.query;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.objects.AbstractObject;
import messif.objects.LocalAbstractObject;
import messif.operations.AbstractOperation;
import messif.operations.OperationErrorCode;

/**
 * Dump structure of an organization to files.
 *
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 */
@AbstractOperation.OperationName("Dump structure of an organization to files")
public class DumpStructureOperation extends AbstractOperation {
    /** Class serial id for serialization */
    private static final long serialVersionUID = 1L;

    //****************** Attributes ******************//

    /** Flag whether object's data will be dumped or just object's ID (locator) */
    private final boolean dumpObjectDescriptors;

    /** Name of the directory to output data to */
    private final String outputFile;
    
    // ***************   Output attribute     ********************* //
    
    private final OutputStream output;
    
    /****************** Constructors ******************/

    /**
     * Creates a new instance of operation.
     * @param dumpObjectDescriptors if true, descriptor (data) instead of object ID is printed
     * @param outputFile path to a file to write all pivots (the file is overwritten)
     */
    @AbstractOperation.OperationConstructor({"objects as descriptors", "output file"})
    public DumpStructureOperation(boolean dumpObjectDescriptors, String outputFile) throws IllegalArgumentException, FileNotFoundException {
        super();
        this.dumpObjectDescriptors = dumpObjectDescriptors;
        this.outputFile = outputFile;
        this.output = new BufferedOutputStream(new FileOutputStream(new File(outputFile)));
    }

    /**
     * Returns argument that was passed while constructing instance.
     * If the argument is not stored within operation, <tt>null</tt> is returned.
     * @param index index of an argument passed to constructor
     * @return argument that was passed while constructing instance
     * @throws IndexOutOfBoundsException if index parameter is out of range
     */
    @Override
    public Object getArgument(int index) throws IndexOutOfBoundsException {
        switch (index) {
        case 0:
            return dumpObjectDescriptors;
        case 1:
            return outputFile;
        default:
            throw new IndexOutOfBoundsException(this.getClass().getSimpleName() + " has only two arguments");
        }
    }

    /**
     * Returns number of arguments that were passed while constructing this instance.
     * @return number of arguments that were passed while constructing this instance
     */
    @Override
    public int getArgumentCount() {
        return 2;
    }

    @Override
    public boolean wasSuccessful() {
        return getErrorCode() == OperationErrorCode.RESPONSE_RETURNED;
    }

    @Override
    public void endOperation() {
        try {
            output.close();
        } catch (IOException ex) {
            Logger.getLogger(DumpStructureOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        endOperation(OperationErrorCode.RESPONSE_RETURNED);
    }
    
    public void addToAnswer(int level, List<String> objectPath, LocalAbstractObject o, float radius) {
        try {
            output.write(o.getLocatorURI().getBytes());
            output.write('\t');
            output.write(getPath(objectPath, level).getBytes());
            output.write('\t');
            output.write(Float.toString(radius).getBytes());
            output.write('\n');
            if (dumpObjectDescriptors) {
                o.write(output, false);
            }
        } catch (IOException ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    private String getPath(List<String> objectPath, int level) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < level; i++) {
            if (sb.length() > 0)
                sb.append('.');
            sb.append(objectPath.get(i));
        }
        return sb.toString();
    }
}
