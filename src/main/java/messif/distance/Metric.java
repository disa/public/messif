/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package messif.distance;

/**
 * This is a marking interface (no methods) saying that a distance fulfills all metric postulates.
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface Metric extends Reflexive, Symmetric, Triangle {
    
}
