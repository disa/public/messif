/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package messif.algorithms.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.algorithms.Algorithm;
import messif.algorithms.AlgorithmMethodException;
import messif.buckets.CapacityFullException;
import messif.buckets.impl.MemoryStorageBucket;
import messif.objects.util.RankedAbstractObject;
import messif.operations.AbstractOperation;
import messif.operations.QueryOperation;
import messif.operations.RankingSingleQueryOperation;
import messif.operations.query.KNNQueryOperation;
import messif.operations.query.RangeQueryOperation;
import messif.statistics.OperationStatistics;
import messif.statistics.Statistics;

/**
 * An algorithm (extension of {@link SequentialScan}) capable of maintaining ground truth information.
 * 
 * The algorithm's method {@link #evaluateGroundTruth} can be called by {@link CoreApplication#operationProcessByMethod) to enrich
 * the last operation's statistics by information retrieval measures computed on the passed (executed) operation and this algorithm's GT result.
 * 
 * Data manipulation operations ({@link InsertOperation}, {@link BulkInsertOperation}) must be used to populate with data used to obtain 
 * the ground truth information.
 * 
 * If caching GT is activated, query operations can be executed to populate the cache with GT information. The cache is 
 * queried by query object locator.
 * 
 * Instantiation of this algorithm is to be done in a config file via algorithmStart and then naming it using {@link CoreApplication#algorithmToNamedInstance}.
 * This name is then used to get it for the call {@link #evaluateGT(messif.operations.RankingSingleQueryOperation, messif.algorithms.impl.GroundTruthEvaluator)}
 * by {@link CoreApplication#operationProcessByMethod}.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class GroundTruthEvaluator extends SequentialScan {
    /** class id for serialization */
    private static final long serialVersionUID = 1L;

    private final Map<String,RankingSingleQueryOperation> gtCache;
    
    /**
     * Creates a new instance of GroundTruthEvaluator as a plain {@link SequentialScan} with {@link MemoryStorageBucket}.
     *
     * @param cacheGTofEvaluatedQueries remembers all evaluated queries in an internal map, so it can be reused by any upcomming {@link QueryOperation} to evaluate
     * 
     * @throws CapacityFullException if the maximal number of buckets is already allocated
     * @throws InstantiationException if <ul><li>the provided storageClass is not a part of LocalBucket hierarchy</li>
     *                                   <li>the storageClass does not have a proper constructor (String,long,long)</li>
     *                                   <li>the correct constructor of storageClass is not accessible</li>
     *                                   <li>the constructor of storageClass has failed</li></ul>
     */
    @Algorithm.AlgorithmConstructor(description = "SequentialScan Access Structure", arguments = {"cacheGTofEvaluatedQueries"})
    public GroundTruthEvaluator(boolean cacheGTofEvaluatedQueries) throws CapacityFullException, InstantiationException {
        super(/*"Ground-Truth Evaluator", */MemoryStorageBucket.class);
        
        if (cacheGTofEvaluatedQueries)
            gtCache = new HashMap<>();
        else
            gtCache = null;
    }

    /**
     * Converts the object to a string representation
     * @return String representation of this algorithm
     */
    @Override
    public String toString() {
        StringBuffer rtv;
        String lineSeparator = System.getProperty("line.separator", "\n");
        
        rtv = new StringBuffer();
        rtv.append("Algorithm: Ground Truth Evaluator via ").append(getName()).append(lineSeparator);
        rtv.append("Bucket Class: ").append(bucket.getClass().getName()).append(lineSeparator);
        rtv.append("Bucket Occupation: ").append(bucket.getOccupation()).append(" bytes").append(lineSeparator);
        rtv.append("Bucket Occupation: ").append(bucket.getObjectCount()).append(" objects").append(lineSeparator);
        
        if (gtCache != null) {
            rtv.append("Cache Occupation: ").append(gtCache.size()).append(" queries").append(lineSeparator);
        }
        
        return rtv.toString();
    }
    
    //**********************************************************
    // POPULATE GT CACHE
    
    @Override
    public void singleQueryObjectSearch(RankingSingleQueryOperation operation) {
        super.singleQueryObjectSearch(operation);

        if (gtCache != null && operation instanceof RankingSingleQueryOperation) {
            String loc = ((RankingSingleQueryOperation)operation).getQueryObject().getLocatorURI();
            gtCache.put(loc, (RankingSingleQueryOperation)operation);
        }
    }
    
    //**********************************************************
    // METHOD TO EVALUATION IR STATISTICS
    
    /** 
     * Evaluates the passed operation with a valid answer against its ground truth answer.
     * 
     * @param oper this argument is automatically populated with "lastOperation" by {@link CoreApplication#operationProcessByMethod}
     * @param alg this argument is automatically populated with the named instance of this class by {@link CoreApplication#operationProcessByMethod}
     * @return instance of oper with possibly modified values
     */
    public static AbstractOperation evaluateGT(RankingSingleQueryOperation oper, GroundTruthEvaluator alg) throws CloneNotSupportedException, AlgorithmMethodException, NoSuchMethodException {
        RankingSingleQueryOperation gt = getGTOper(oper, alg);
        
        // Make the comparison
        if (oper instanceof KNNQueryOperation)
            evaluateKNN((KNNQueryOperation)oper, (KNNQueryOperation)gt);
        else if (oper instanceof RangeQueryOperation)
            evaluateRange((RangeQueryOperation)oper, (RangeQueryOperation)gt);
        
        return oper;
    }
    
    private static void evaluateKNN(KNNQueryOperation oper, KNNQueryOperation gt) {
        Set<String> locsCommon = getLocatorsInAnswerIntersection(oper, gt);
        
        setRecallPrecision((float)locsCommon.size() / (float)oper.getAnswerCount(), 
                           (float)locsCommon.size() / (float)oper.getAnswerCount());
    }

    private static void evaluateRange(RangeQueryOperation oper, RangeQueryOperation gt) {
        Set<String> locsCommon = getLocatorsInAnswerIntersection(oper, gt);
        final int operSize = oper.getAnswerCount();
        final int gtSize = getRangeAnswerCount(gt, (operSize == 0) ? 0 : oper.getAnswerDistance());
        
        float recall = (operSize == 0) ? 0 : (float)locsCommon.size() / (float)operSize;
        if (operSize == 0 && locsCommon.isEmpty())      // if nothing to compare, use 100 % recall.
            recall = 1;
        float precision = (gtSize == 0) ? 0 : (float)locsCommon.size() / (float)gtSize;
        
        setRecallPrecision(recall, precision);
    }    
    
    private static int getRangeAnswerCount(RangeQueryOperation gt, float answerDistance) {
        Iterator<RankedAbstractObject> it = gt.getAnswer();
        int size = 0;
        while (it.hasNext()) {
            RankedAbstractObject ro = it.next();
            if (ro.getDistance() <= answerDistance)
                size++;
            else
                break;
        }
        return size;
    }
    
    private static Set<String> getLocators(RankingSingleQueryOperation op, Predicate<RankedAbstractObject> flt) {
        Set<String> locs = new HashSet<>();
        Iterator<RankedAbstractObject> it = op.getAnswer();
        while (it.hasNext()) {
            RankedAbstractObject ro = it.next();
            if (flt == null || flt.test(ro))
                locs.add(ro.getObject().getLocatorURI());
            else
                break;
        }
        return locs;
    }
    
    private static Set<String> getLocatorsInAnswerIntersection(RankingSingleQueryOperation oper, RankingSingleQueryOperation gt) {
        final float maxDist = (oper.getAnswerCount() == 0) ? 0 : oper.getAnswerDistance();
        Set<String> locsOper = getLocators(oper, null);
        Set<String> locsGt = getLocators(gt, new Predicate<RankedAbstractObject>() {
            @Override
            public boolean test(RankedAbstractObject ro) {
                return (ro.getDistance() <= maxDist);
            }
        });
        locsOper.retainAll(locsGt);
        return locsOper;
    }

    private static RankingSingleQueryOperation getGTOper(RankingSingleQueryOperation operToEval, GroundTruthEvaluator alg) throws CloneNotSupportedException, AlgorithmMethodException, NoSuchMethodException {
        String queryLoc = operToEval.getQueryObject().getLocatorURI();
        RankingSingleQueryOperation operGT = null;
        
        if (alg.gtCache != null) {
            operGT = alg.gtCache.get(queryLoc);
        }
        if (operGT == null) {   // if no cache or not present in the cache
            operGT = (RankingSingleQueryOperation) operToEval.clone();
            boolean globalStats = Statistics.isEnabledGlobally();
            if (globalStats)
                Statistics.disableGlobally();
            //alg.execute(false, operGT);     // calls search() method internally, which stores the query in the cache if requested.
            try {
                alg.search(operGT);
            } catch (InterruptedException ex) {
                Logger.getLogger(GroundTruthEvaluator.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (globalStats)
                Statistics.enableGlobally();
        }
        //addGTOper(operToEval, alg); // store to cache if we should
        
        return operGT;
    }

    private static void addGTOper(RankingSingleQueryOperation operGT, GroundTruthEvaluator alg) {
        if (alg.gtCache == null)
            return;
        String queryLoc = operGT.getQueryObject().getLocatorURI();
        alg.gtCache.put(queryLoc, operGT);
    }
    
    private static void setRecallPrecision(float recall, float precision) {
        OperationStatistics s = OperationStatistics.getLocalThreadStatistics();
        s.getStatisticCounter("OperationRecall").set((long) (100f * recall));
        s.getStatisticCounter("OperationPrecision").set((long) (100f * precision));
    }
}
