/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */

package messif.objects.impl;


import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import messif.objects.LocalAbstractObject;
import messif.objects.nio.BinaryInput;
import messif.objects.nio.BinarySerializator;


/**
 * This object uses static array of floats as its data content. The data is serialized in a special way fitting 
 *  the neural network output (Caffe).
 * No implementation of distance function is provided.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ObjectFloatVectorNeuralNetworkL2 extends ObjectFloatVectorNeuralNetwork {
    
    /** class id for serialization */
    private static final long serialVersionUID = 450401L;

   //****************** Constructors ******************//

    /**
     * Creates a new instance of ObjectFloatVector.
     * @param data the data content of the new object
     */
    public ObjectFloatVectorNeuralNetworkL2(float[] data) {
        super(data);
    }

    /**
     * Creates a new instance of ObjectFloatVector.
     * @param locatorURI the locator URI for the new object
     * @param data the data content of the new object
     */
    public ObjectFloatVectorNeuralNetworkL2(String locatorURI, float[] data) {
        super(locatorURI, data);
    }

    /**
     * Creates a new instance of ObjectFloatVector from text stream.
     * @param stream the stream from which to read lines of text
     * @throws EOFException if the end-of-file of the given stream is reached
     * @throws IOException if there was an I/O error during reading from the stream
     * @throws NumberFormatException if a line read from the stream does not consist of comma-separated or space-separated numbers
     */
    public ObjectFloatVectorNeuralNetworkL2(BufferedReader stream) throws EOFException, IOException, NumberFormatException {
        super(stream);
    }
    
    @Override
    protected float getDistanceImpl(LocalAbstractObject obj, float distThreshold) {
        float [] objdata = ((ObjectFloatVectorNeuralNetwork)obj).data;
        if (objdata.length != data.length)
            throw new IllegalArgumentException("Cannot compute distance on different vector dimensions (" + data.length + ", " + objdata.length + ")");

        float powSum = 0;
        for (int i = 0; i < data.length; i++) {
            float dif = (data[i] - objdata[i]);
            powSum += dif * dif;
        }

        return (float)Math.sqrt(powSum);
    }

    //************ BinarySerializable interface ************//

    /**
     * Creates a new instance of ObjectFloatVectorL2 loaded from binary input buffer.
     *
     * @param input the buffer to read the ObjectFloatVector from
     * @param serializator the serializator used to write objects
     * @throws IOException if there was an I/O error reading from the buffer
     */
    protected ObjectFloatVectorNeuralNetworkL2(BinaryInput input, BinarySerializator serializator) throws IOException {
        super(input, serializator);
    }
    
}
