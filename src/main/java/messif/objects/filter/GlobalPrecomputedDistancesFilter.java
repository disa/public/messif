/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package messif.objects.filter;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import messif.objects.LocalAbstractObject;
import messif.objects.PrecomputedDistancesFilter;
import messif.objects.PrecomputedDistancesFilterFactory;
import messif.objects.nio.BinaryInput;
import messif.objects.nio.BinaryOutput;
import messif.objects.nio.BinarySerializator;

/**
 * A service-like implementation of precomputed distances.
 * It can be used as a fast substitute at objects to provide a global cache of already computed distances.
 * Remark: Do not cache the distances calculated to external data (e.g., query objects) not present in your indexes database, since it may bloat.
 * 
 * TODO: Current implementation ignores meta distances.
 *
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 */
public class GlobalPrecomputedDistancesFilter extends PrecomputedDistancesFilter {
    
    /** Class serial id for serialization */
    private static final long serialVersionUID = 1L;

    /** Key is a compound of object locators for which the distance is stored in the value. */
    private static final Map<String,Float> cache = Collections.synchronizedMap(new HashMap<String,Float>());;

    /** A reference to an object to which this instance is registered to */
    private final LocalAbstractObject objReg;
    
    /** Instance of this filter's factory */
    public static final PrecomputedDistancesFilterFactory<GlobalPrecomputedDistancesFilter> CREATOR = new PrecomputedDistancesFilterFactory<GlobalPrecomputedDistancesFilter>() {
        @Override
        public GlobalPrecomputedDistancesFilter createPrecomputedDistancesFilter(LocalAbstractObject obj) {
            return new GlobalPrecomputedDistancesFilter(obj);
        }

        @Override
        public Class<? extends GlobalPrecomputedDistancesFilter> getPrecomputedDistancesFilterClass() {
            return GlobalPrecomputedDistancesFilter.class;
        }
    };
    
    /**
     * Creates a new instance of PrecomputedDistancesPivotMapFilter
     */
    public GlobalPrecomputedDistancesFilter() {
        objReg = null;      // A global instanace
    }

    /**
     * Creates a new instance of GlobalPrecomputedDistancesFilter and attaches it to the object
     * @param object the object to which to add this filter
     */
    public GlobalPrecomputedDistancesFilter(LocalAbstractObject object) {
        objReg = object;
        object.chainFilter(this, true);
    }
   
    /**
     * Returns the number of stored precomputed distances.
     * @return the number of stored precomputed distances
     */
    public int getPrecompDistSize() {
        return cache.size();
    }
    
    private String getKey(LocalAbstractObject obj1, LocalAbstractObject obj2) {
        String loc1 = obj1.getLocatorURI();
        String loc2 = obj2.getLocatorURI();
        String key = (loc1.compareTo(loc2) <= 0) ? loc1 + "|" + loc2 : loc2 + "|" + loc1;
        return key;
    }
    
    public boolean addPrecomputedDistance(LocalAbstractObject obj1, LocalAbstractObject obj2, float distance, float[] metaDistances) {
        String key = getKey(obj1, obj2);
        return (cache.put(key, distance) == null);
    }

    public float getPrecomputedDistance(LocalAbstractObject obj1, LocalAbstractObject obj2, float[] metaDistances) {
        String key = getKey(obj1, obj2);
        Float distance = cache.get(key);

        return (distance == null) ? LocalAbstractObject.UNKNOWN_DISTANCE : distance;
    }

    /** 
     * Forget all stored precomputed distances in the cache.
     */
    public void clear() {
        cache.clear();
    }

    //*************************************************
    // Precomputed Distances Filter interface
    //*************************************************

    @Override
    protected boolean addPrecomputedDistance(LocalAbstractObject obj, float distance, float[] metaDistances) {
        return addPrecomputedDistance(objReg, obj, distance, metaDistances);
    }

    @Override
    public float getPrecomputedDistance(LocalAbstractObject obj, float[] metaDistances) {
        return getPrecomputedDistance(objReg, obj, metaDistances);
    }

    @Override
    public boolean excludeUsingPrecompDist(PrecomputedDistancesFilter targetFilter, float radius) {
        return false;
//        try {
//            return excludeUsingPrecompDist((GlobalPrecomputedDistancesFilter)targetFilter, radius);
//        } catch (ClassCastException e) {
//            return false;
//        }
    }

//    /**
//     * Return true if the obj has been filtered out using stored precomputed distance.
//     * Otherwise returns false, i.e. when obj must be checked using original distance (getDistance()).
//     *
//     * In other words, method returns true if this object and obj are more distant than radius. By
//     * analogy, returns false if this object and obj are within distance radius. However, both this cases
//     * use only precomputed distances! Thus, the real distance between this object and obj can be greater
//     * than radius although the method returned false!!!
//     *
//     * @param targetFilter the target precomputed distances
//     * @param radius the radius to check the precomputed distances for
//     * @return <tt>true</tt> if object associated with <tt>targetFilter</tt> filter can be excluded (filtered out) using this precomputed distances
//     */
//    public boolean excludeUsingPrecompDist(GlobalPrecomputedDistancesFilter targetFilter, float radius) {
//        for (Map.Entry<String, Float> entry : cache.entrySet()) {
//            Float targetDistance = targetFilter.cache.get(entry.getKey());
//            if (targetDistance != null)
//                continue;
//            if (Math.abs(entry.getValue().floatValue() - targetDistance.floatValue()) > radius)
//                return true;
//        }
//
//        return false;
//    }
    
    @Override
    public boolean includeUsingPrecompDist(PrecomputedDistancesFilter targetFilter, float radius) {
        return false;
//        try {
//            return includeUsingPrecompDist((GlobalPrecomputedDistancesFilter)targetFilter, radius);
//        } catch (ClassCastException e) {
//            return false;
//        }
    }

//    /**
//     * Returns <tt>true</tt> if object associated with <tt>targetFilter</tt> filter can be included using this precomputed distances.
//     * See {@link messif.objects.LocalAbstractObject#includeUsingPrecompDist} for full explanation.
//     *
//     * @param targetFilter the target precomputed distances
//     * @param radius the radius to check the precomputed distances for
//     * @return <tt>true</tt> if object associated with <tt>targetFilter</tt> filter can be included using this precomputed distances
//     */
//    public boolean includeUsingPrecompDist(GlobalPrecomputedDistancesFilter targetFilter, float radius) {
//        for (Map.Entry<String, Float> entry : cache.entrySet()) {
//            Float targetDistance = targetFilter.cache.get(entry.getKey());
//            if (targetDistance != null)
//                continue;
//            if (entry.getValue().floatValue() + targetDistance.floatValue() <= radius)
//                return true;
//        }
//
//        return false;
//    }

    @Override
    protected void writeData(OutputStream stream) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    protected boolean isDataWritable() {
        return false;
    }
    
    //****************** Cloning ******************//

    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("PrecomputedDistancesPivotMapFilter can't be cloned");
    }


    //************ BinarySerializable interface ************//

    /**
     * Creates a new instance of PrecomputedDistancesPivotMapFilter loaded from binary input.
     * 
     * @param input the input to read the PrecomputedDistancesPivotMapFilter from
     * @param serializator the serializator used to write objects
     * @throws IOException if there was an I/O error reading from the input
     */
    protected GlobalPrecomputedDistancesFilter(BinaryInput input, BinarySerializator serializator) throws IOException {
        super(input, serializator);
        int items = serializator.readInt(input);
        
        if (items == -1) {
            // An object's (local) filter
            objReg = serializator.readObject(input, LocalAbstractObject.class);
        } else {
            objReg = null;
            cache.clear();
            for (int i = 0; i < items; i++)
                cache.put(serializator.readString(input), serializator.readFloat(input));
        }
    }

    /**
     * Binary-serialize this object into the <code>output</code>.
     * @param output the output that this object is binary-serialized into
     * @param serializator the serializator used to write objects
     * @return the number of bytes actually written
     * @throws IOException if there was an I/O error during serialization
     */
    @Override
    public int binarySerialize(BinaryOutput output, BinarySerializator serializator) throws IOException {
        int size = super.binarySerialize(output, serializator);
        
        if (objReg == null) {
            size += serializator.write(output, cache.size());
            for (Map.Entry<String, Float> entry : cache.entrySet()) {
                size += serializator.write(output, entry.getKey());
                size += serializator.write(output, entry.getValue().floatValue());
            }
        } else {
            size += serializator.write(output, (int)-1);
            size += serializator.write(output, objReg);
        }
        
        return size;
    }

    /**
     * Returns the exact size of the binary-serialized version of this object in bytes.
     * @param serializator the serializator used to write objects
     * @return size of the binary-serialized version of this object
     */
    @Override
    public int getBinarySize(BinarySerializator serializator) {
        int size = super.getBinarySize(serializator) + Integer.SIZE / 8;
        
        if (objReg == null) {
            for (String key : cache.keySet())
                size += serializator.getBinarySize(key) + Float.SIZE / 8;
        } else {
            size += serializator.getBinarySize(objReg);
        }
        
        return size;
    }
}
