/**
 * Distance computation filters -- caches for precomputed distances.
 */
package messif.objects.filter;
