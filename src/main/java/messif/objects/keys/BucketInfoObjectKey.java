/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package messif.objects.keys;

import java.io.IOException;
import java.io.Serializable;
import messif.objects.nio.BinaryOutput;
import messif.objects.nio.BinarySerializator;

/**
 * Carries information about bucket ID and relative order of the bucket as it got accessed during query evaluation, for example.
 * 
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 */
public class BucketInfoObjectKey extends BucketIdObjectKey implements Serializable {
    /** Class serial id for serialization. */
    private static final long serialVersionUID = 1L;

    private int bucketAccessOrderNo;  
    
    public BucketInfoObjectKey(String locatorURI, int bucketId, int accessOrderNo) {
        super(locatorURI, bucketId);
        this.bucketAccessOrderNo = accessOrderNo;
    }

    public int getBucketAccessOrderNo() {
        return bucketAccessOrderNo;
    }

    @Override
    public String toString() {
        return super.toString() + ":" + bucketAccessOrderNo;
    }

    @Override
    public int getBinarySize(BinarySerializator serializator) {
        return super.getBinarySize(serializator) + serializator.getBinarySize(bucketAccessOrderNo);
    }

    @Override
    public int binarySerialize(BinaryOutput output, BinarySerializator serializator) throws IOException {
        return super.binarySerialize(output, serializator) + serializator.write(output, bucketAccessOrderNo);
    }

    @Override
    public BucketIdObjectKey clone(String locatorURI) throws CloneNotSupportedException {
        BucketInfoObjectKey key = (BucketInfoObjectKey) super.clone(locatorURI);
        key.bucketAccessOrderNo = bucketAccessOrderNo;
        return key;
    }
        
    /** Replace this class with the new version of it */
    public Object readResolve() {
        return new messif.objects.keys.BucketInfoObjectKey(this.getLocatorURI(), this.getBucketId(), this.getBucketAccessOrderNo());
    }
    
    
}
