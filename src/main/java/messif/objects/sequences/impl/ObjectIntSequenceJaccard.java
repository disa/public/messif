/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package messif.objects.sequences.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import messif.objects.LocalAbstractObject;
import messif.objects.nio.BinaryInput;
import messif.objects.nio.BinarySerializator;
import messif.objects.text.impl.JaccardDistanceFunction;
import messif.utility.SortingIterator;

/**
 * Sequence of integers compared by Jaccard (coefficient) distance.
 * 
 * TODO: Use implemenation of {@link JaccardDistanceFunction} and {@link SortingIterator}?
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Prochazka, Masaryk University, Brno, Czech Republic, xprocha6@fi.muni.cz
 */
public class ObjectIntSequenceJaccard extends ObjectIntSequence {

    public ObjectIntSequenceJaccard(BufferedReader stream) throws IOException, NumberFormatException {
        super(stream);
    }

    public ObjectIntSequenceJaccard(BinaryInput input, BinarySerializator serializator) throws IOException {
        super(input, serializator);
    }
    
    /**
     * Based on https://commons.apache.org/proper/commons-text/apidocs/src-html/org/apache/commons/text/similarity/JaccardSimilarity.html
     */
    @Override
    protected float getDistanceImpl(LocalAbstractObject obj, float distThreshold) {
        final int[] left = data;
        final int[] right = ((ObjectIntSequenceJaccard) obj).data;

        if (left.length == 0 && right.length == 0) {        // same objects
            return 0.0F;
        }
        if (left.length == 0 || right.length == 0) {        // completely different ones
            return 1.0F;
        }

        final Set<Integer> leftSet = (Set<Integer>)((Set)Arrays.stream(left).boxed().collect(Collectors.toSet()));
        final Set<Integer> rightSet = (Set<Integer>)((Set)Arrays.stream(right).boxed().collect(Collectors.toSet()));

        final Set<Integer> unionSet = new HashSet<>(leftSet);
        unionSet.addAll(rightSet);

        final int intersectionSize = leftSet.size() + rightSet.size() - unionSet.size();

        return 1.0F - ((float)intersectionSize / (float)unionSet.size());
    }
}
