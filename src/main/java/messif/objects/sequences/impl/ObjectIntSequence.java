/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package messif.objects.sequences.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import messif.objects.LocalAbstractObject;
import messif.objects.impl.ObjectIntVector;
import messif.objects.nio.BinaryInput;
import messif.objects.nio.BinaryOutput;
import messif.objects.nio.BinarySerializable;
import messif.objects.nio.BinarySerializator;

/**
 * This object encapsulates a variable length array of integers as its data content.
 * No implementation of distance function is provided - see {@link ObjectIntSequenceJaccard}.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Prochazka, Masaryk University, Brno, Czech Republic, xprocha6@fi.muni.cz
 */
public abstract class ObjectIntSequence extends LocalAbstractObject implements BinarySerializable {
    /** class id for serialization */
    private static final long serialVersionUID = 1L;

    //****************** Attributes ******************//

    /** Data array */
    protected int[] data;


    //****************** Constructors ******************//

    protected ObjectIntSequence(BufferedReader stream) throws IOException, NumberFormatException {
        String line = readObjectComments(stream);
        data = ObjectIntVector.parseIntVector(line);
    }

    //****************** Text file store/retrieve methods ******************//

    @Override
    protected void writeData(OutputStream stream) throws IOException {
        ObjectIntVector.writeIntVector(data, stream, ' ', '\n');
    }
    
    //****************** Equality comparing function ******************

    @Override
    public boolean dataEquals(Object obj) {
        if (!(obj instanceof ObjectIntSequence))
            return false;

        return Arrays.equals(((ObjectIntSequence) obj).data, data);
    }

    @Override
    public int dataHashCode() {
        return Arrays.hashCode(data);
    }

    //****************** Attribute access methods ******************//
    
    public Integer getSequenceItem(int index) {
        return this.data[index];
    }

    public int getSequenceItemInt(int index) {
        return this.data[index];
    }
    
    public int getSequenceLength() {
        return this.data.length;
    }
    
//    public <S> S[] getVectorData(S[] template) {
//        if (template != null && this.data != null && template.getClass().isAssignableFrom(this.data.getClass()))
//            return (S[])(Object)(this.data.clone());
//        return null;
//    }

    /**
     * Returns the vector of integer values, which represents the contents of this object.
     * A copy is returned, so any modifications to the returned array do not affect the original object.
     * @return the data contents of this object
     */
    public int[] getSequenceData() {
        return this.data.clone();
    }

    @Override
    public int getSize() {
        return this.data.length * Integer.SIZE / 8;
    }

    //************ String representation ************//

    /**
     * Converts this object to a string representation.
     * The format is the comma-separated list of coordinates enclosed in square brackets
     * and the result of <code>super.toString()</code> is appended.
     */
    @Override
    public String toString() {
        StringBuffer rtv = new StringBuffer(super.toString()).append(" [");

        for (int i = 0; i < this.data.length; i++) {
            if (i > 0) rtv.append(", ");
            rtv.append(data[i]);
        }
        rtv.append("]");

        return rtv.toString();
    }


    //************ BinarySerializable interface ************//

    /**
     * Creates a new instance of ObjectIntVector loaded from binary input buffer.
     * 
     * @param input the buffer to read the ObjectIntVector from
     * @param serializator the serializator used to write objects
     * @throws IOException if there was an I/O error reading from the buffer
     */
    protected ObjectIntSequence(BinaryInput input, BinarySerializator serializator) throws IOException {
        super(input, serializator);
        data = serializator.readIntArray(input);
    }

    @Override
    public int binarySerialize(BinaryOutput output, BinarySerializator serializator) throws IOException {
        return super.binarySerialize(output, serializator) +
               serializator.write(output, data);
    }

    @Override
    public int getBinarySize(BinarySerializator serializator) {
        return super.getBinarySize(serializator) + serializator.getBinarySize(data);
    }
}
