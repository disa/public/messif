/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.objects.text.impl;

import java.io.Serializable;
import messif.objects.DistanceFunction;
import messif.utility.IteratorIntersection;
import messif.utility.SortingIterator;

/**
 * Computes the cosine distance of the two objects.
 * Please note that this class is abstract: a method that returns the comparing items
 * as well as a method that provides the weights must be implemented.
 *
 * @param <T> the type of items for the Jaccard distance function
 * @param <O> the type of the distance function arguments
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public abstract class JaccardDistanceFunction<T, O> implements DistanceFunction<O>, Serializable {
    
    /** Class id for serialization. */
    private static final long serialVersionUID = 1L;    
    
    /**
     * Create iterator over items in the given {@code obj}.
     * Please note that the returned iterator must provide <em>sorted</em> items.
     * @param obj the object the items of which to compare by cosine distance
     * @return an iterator over items in the given {@code obj}
     */
    protected abstract SortingIterator<T> getValuesIterator(O obj);

    /**
     * Returns a weight for the given item.
     * By default, this method returns 1 for any item.
     * @param item the item for which to get the weight
     * @param obj the object from which the item originates
     * @return a weight for the given item
     */
    protected float getWeight(T item, O obj) {
        return 1f;
    }

    @Override
    public float getDistance(O o1, O o2) {
        SortingIterator<T> o1Iterator = getValuesIterator(o1);
        SortingIterator<T> o2Iterator = getValuesIterator(o2);

        IteratorIntersection<T> intersection = new IteratorIntersection<T>(o1Iterator, o2Iterator, o1Iterator);
        if (!intersection.step()) // No data in one of the iterators, return max distance
            return 1;

        // Initialize computing variables
        double numerator = 0;
        double denominator = 0;

        do {
            // Update weight of the value from iterator 1 if advanced
            if (intersection.hasIterator1Advanced()) {
                double weight = getWeight(intersection.getValue1(), o1);
                denominator += weight;
                if (intersection.isIntersecting())
                    numerator += weight;
            }
            // Update weight of the value from iterator 2 if advanced
            if (intersection.hasIterator2Advanced()) {
                double weight = getWeight(intersection.getValue2(), o2);
                denominator += weight;
                if (intersection.isIntersecting())
                    numerator += weight;
            }
        } while (intersection.step());

        double distance = 1.0 - (numerator / denominator);
        // Compensate for float sizing error
        if (distance < 0.0000001)
            return 0;
        return (float)distance;
    }
}
